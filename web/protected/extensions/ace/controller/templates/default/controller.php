<?php
/**
 * This is the template for generating a controller class file.
 * The following variables are available in this template:
 * - $this: the ControllerCode object
 */
?>
<?php echo "<?php\n"; ?>

/**
 * @author
 *
 */
class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseClass."\n"; ?>
{


    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='<?php foreach($this->getActionIDs() as $action): ?><?php echo $action; break; ?><?php endforeach; ?>';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de <?php echo $this->controllerClass; ?>',
        'write' => 'Creación y Modificación de <?php echo $this->controllerClass; ?>',
        'admin' => 'Administración Completa  de <?php echo $this->controllerClass; ?>',
        'label' => 'Peticiones de <?php echo $this->controllerClass; ?>'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array(<?php foreach($this->getActionIDs() as $action): ?><?php echo "'$action', "; ?><?php endforeach; ?>),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array(<?php foreach($this->getActionIDs() as $action): ?><?php echo "'$action', "; ?><?php endforeach; ?>),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array(),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


<?php foreach($this->getActionIDs() as $action): ?>
	
       /**
        * 
        *
        */
        public function action<?php echo ucfirst($action); ?>() {
		$this->render('<?php echo $action; ?>');
	}

<?php endforeach; ?>
	// Uncomment the following methods and override them if needed
	/*
	public function actions() {
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
