<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;

?>

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/styles-iview.css'; ?>" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/iview.css'; ?>" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.'/public/css/skin-4/style.css'; ?>" />

    <div class="col-xs-12">
        <div class="row row-fluid">
            <div class="accordion-style1 panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                <i data-icon-show="icon-angle-right" data-icon-hide="icon-angle-down" class="bigger-110 icon-angle-down"></i>
                                &nbsp;AVISO IMPORTANTE 01
                            </a>
                        </h4>
                    </div>

                    <div id="collapseOne" class="panel-collapse in" style="height: auto;">
                        <div class="panel-body">
                            <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                                Noticia 01
                            </p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle collapsed">
                                <i data-icon-show="icon-angle-right" data-icon-hide="icon-angle-down" class="bigger-110 icon-angle-right"></i>
                                &nbsp;AVISO IMPORTANTE 02
                            </a>
                        </h4>
                    </div>

                    <div id="collapseTwo" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <p style="text-align: justify; font-size:15px;font-family: 'Helvetica';" > 
                                Noticia 02
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/raphael-min.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.easing.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.fullscreen.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/iview.js',CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/slider.js',CClientScript::POS_END); ?>
